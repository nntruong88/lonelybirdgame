﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public delegate void GameControllerDelegate();
    public static event GameControllerDelegate OnGameStarted;
    public static event GameControllerDelegate OnGameoverConfirmed;

    public static GameController Instance;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text scoreGameOverText;

    [SerializeField]
    private GameObject startPage;

    [SerializeField]
    private GameObject gameOverPage;

    [SerializeField]
    private GameObject countdownPage;

    enum PageState
    {
        None,
        Start,
        GameOver,
        CountDown
    }
    private PageState pageState;

    int score = 0;

    bool gameOver = false;
    public bool GameOver
    {
        get { return gameOver; }
        set { gameOver = value; }
    }

    bool pauseGame = false;
    public bool PauseGame
    {
        get { return pauseGame; }
        set { pauseGame = value; }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        CountdownText.OnCountDownFinished += OnCountDownFinished;
        BirdController.OnBirdScored += OnBirdScored;
        BirdController.OnBirdDied += OnBirdDied;
    }

    private void OnDisable()
    {
        CountdownText.OnCountDownFinished -= OnCountDownFinished;
        BirdController.OnBirdScored -= OnBirdScored;
        BirdController.OnBirdDied -= OnBirdDied;
    }

    void SetPageState(PageState state)
    {
        
        switch (state)
        {
            case PageState.None:
                scoreText.gameObject.SetActive(true);
                scoreText.text = score.ToString();
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countdownPage.SetActive(false);
                break;

            case PageState.Start:
                scoreText.gameObject.SetActive(false);
                startPage.SetActive(true);
                gameOverPage.SetActive(false);
                countdownPage.SetActive(false);
                break;

            case PageState.GameOver:
                scoreText.gameObject.SetActive(false);
                scoreGameOverText.text = "Score:" + score.ToString();
                startPage.SetActive(false);
                gameOverPage.SetActive(true);
                countdownPage.SetActive(false);
                break;

            case PageState.CountDown:
                scoreText.gameObject.SetActive(false);
                startPage.SetActive(false);
                gameOverPage.SetActive(false);
                countdownPage.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void ConfirmGameOver()
    {
        //actived when replay button is hit
        OnGameoverConfirmed();//event
        SetPageState(PageState.Start);
    }

    public void StartGame()
    {
        //actived when play button is hit
        SetPageState(PageState.CountDown);
    }

    private void Reset()
    {
        score = 0;
        gameOver = false;
        pauseGame = false;
    }

    void OnCountDownFinished()
    {
        Reset();
        SetPageState(PageState.None);
        OnGameStarted();
    }

    void OnBirdScored()
    {
        score++;
        scoreText.text = score.ToString();
    }

    void OnBirdDied()
    {
        gameOver = true;
        int saveScore = PlayerPrefs.GetInt("HighScore");
        if(score > saveScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        SetPageState(PageState.GameOver);
    }
}
