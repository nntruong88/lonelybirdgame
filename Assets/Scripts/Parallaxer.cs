﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxer : MonoBehaviour {

	class PoolObject
    {
        public Transform trans;
        public bool inUse;
        public PoolObject(Transform t)
        {
            trans = t;
        }
        public void Use()
        {
            inUse = true;
        }
        public void Dispose()
        {
            inUse = false;
        }
    }

    [System.Serializable]
    public struct YSpawnRange
    {
        public float min;
        public float max;
    }
    public GameObject Prefab;
    public int poolSize;
    public float shiftSpeed;
    public float spawnRate;

    public YSpawnRange ySpawnRange;
    public bool spawnImmediate;//particle prewarm
    public Vector3 immediateSpawnPos;
    public int spaceMultiX = 3;

    float spawnTimer;

    PoolObject[] poolObjects;
    GameController game;

    private void Awake()
    {
        Configure();
    }

    private void Start()
    {
        game = GameController.Instance;
    }

    private void OnEnable()
    {
        GameController.OnGameoverConfirmed += OnGameoverConfirmed;
    }

    private void OnDisable()
    {
        GameController.OnGameoverConfirmed -= OnGameoverConfirmed;
    }

    void OnGameoverConfirmed()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].Dispose();
            poolObjects[i].trans.position = Vector3.one * 1000;
        }
        if (spawnImmediate)
            SpawnImmediate();
    }

    private void Update()
    {
        if (game.PauseGame || game.GameOver)
            return;
        Shift();
        spawnTimer += Time.deltaTime;
        if(spawnTimer > spawnRate)
        {
            Spawn();
            spawnTimer = 0;
        }
    }

    void Configure()
    {
        if (poolSize <= 0)
            return;
        poolObjects = new PoolObject[poolSize];
        for (int i = 0; i < poolObjects.Length; i++)
        {
            Vector3 scale = Prefab.transform.localScale;
            GameObject go = Instantiate(Prefab) as GameObject;
            Transform t = go.transform;
            t.SetParent(transform);
            t.localScale = scale;
            t.position = new Vector3(Camera.main.aspect * spaceMultiX, 0, 0);// Vector3.one * 1000;
            poolObjects[i] = new PoolObject(t);
        }

        if(spawnImmediate)
        {
            SpawnImmediate();
        }
    }

    void Spawn()
    {
        Transform t = GetPoolObject();
        if (t == null)
            return;//if true, this indicates that poolSize is too small
        Vector3 pos = Vector3.zero;
        pos.x = Camera.main.aspect * spaceMultiX/2;
        pos.y = Random.Range(ySpawnRange.min, ySpawnRange.max);
        t.localPosition = pos;
    }

    void SpawnImmediate()
    {
        Transform t = GetPoolObject();
        if (t == null)
            return;//if true, this indicates that poolSize is too small
        Vector3 pos = Vector3.zero;
        pos.x = immediateSpawnPos.x;
        pos.y = Random.Range(ySpawnRange.min, ySpawnRange.max);
        t.localPosition = pos;
        Spawn();
    }

    void Shift()
    {
        int len = poolObjects.Length;
        PoolObject poolObj;
        for (int i = 0; i < len; i++)
        {
            poolObj = poolObjects[i];
            if (poolObj == null)
                continue;
            poolObj.trans.position += -Vector3.right * shiftSpeed * Time.deltaTime;
            CheckDisposeObject(poolObj);
        }
    }

    void CheckDisposeObject(PoolObject poolObject)
    {
        if(poolObject.trans.position.x < -(Camera.main.aspect * spaceMultiX) /*defaultSpawnPos.x*/)
        {
            poolObject.Dispose();
            poolObject.trans.position = Vector3.one * 1000;
        }
    }

    Transform GetPoolObject()
    {
        PoolObject poolObj = null;
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObj = poolObjects[i];
            if (!poolObj.inUse)
            {
                poolObj.Use();
                return poolObj.trans;
            }
        }
        return null;
    }
}
