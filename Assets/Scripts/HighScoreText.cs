﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HighScoreText : MonoBehaviour {

    [SerializeField] Text highScore;
    private void OnEnable()
    {
        highScore = GetComponent<Text>();
        highScore.text = "HighScore: " + PlayerPrefs.GetInt("HighScore").ToString();
    }
}
