﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour {
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnBirdDied;
    public static event PlayerDelegate OnBirdScored;

    [SerializeField]
    private Rigidbody2D rigidbody;

    [SerializeField]
    private Quaternion downRotation;

    [SerializeField]
    private Quaternion forwardRotation;

    [SerializeField]
    private float tapForce = 10;

    [SerializeField]
    private float smooth = 5;

    [SerializeField]
    private Vector3 startPos;

    [SerializeField]
    private Vector3 diedPos;

    [SerializeField]
    private AudioSource tapAudio;

    [SerializeField]
    private AudioSource scoreAudio;

    [SerializeField]
    private AudioSource gameOverAudio;

    private GameController gameController;
    bool pipeHit;

    // Use this for initialization
    void Start () {
        if (rigidbody == null)
            rigidbody = GetComponent<Rigidbody2D>();
        downRotation = Quaternion.Euler(0, 0, -90);
        forwardRotation = Quaternion.Euler(0, 0, 35);
        rigidbody.simulated = true;
        startPos = this.transform.position;
        if (gameController == null)
            gameController = GameController.Instance;
    }

    private void OnEnable()
    {
        GameController.OnGameStarted += OnGameStarted;
        GameController.OnGameoverConfirmed += OnGameoverConfirmed;
    }

    private void OnDisable()
    {
        GameController.OnGameStarted -= OnGameStarted;
        GameController.OnGameoverConfirmed -= OnGameoverConfirmed;
    }

    void OnGameStarted()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.simulated = true;
    }

    void OnGameoverConfirmed()
    {
        transform.position = startPos;
        transform.rotation = Quaternion.identity;
    }

    // Update is called once per frame
    void Update () {
        if(gameController != null)
        {
            if (gameController.PauseGame)
                return;
            if (gameController.GameOver)
                return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            tapAudio.Play();
            this.transform.rotation = forwardRotation;
            rigidbody.velocity = Vector2.zero;
            rigidbody.AddForce(Vector2.up * tapForce);
        }
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, downRotation, smooth * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "ScoreZone")
        {
            scoreAudio.Play();
            OnBirdScored();//event sent to GameController
        }

        if(collider.gameObject.tag == "PipeZone")
        {
            gameController.PauseGame = true;
            Vector3 pos = this.transform.position;
            pos.y = diedPos.y;
            rigidbody.AddForce(pos, ForceMode2D.Force);
        }

        if (collider.gameObject.tag == "DeadZone")
        {
            gameOverAudio.Play();
            OnBirdDied(); //event sent to GameController
            rigidbody.simulated = false;
            this.transform.rotation = Quaternion.identity;
        }
    }
}
