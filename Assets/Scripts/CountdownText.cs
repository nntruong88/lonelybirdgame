﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CountdownText : MonoBehaviour {

    public delegate void CountDownFinished();
    public static event CountDownFinished OnCountDownFinished;

    public int numCountDown = 3;

    [SerializeField]
    private Text countdown;

    private void OnEnable()
    {
        if(countdown == null)
            countdown = GetComponent<Text>();
        countdown.text = numCountDown.ToString();
        StartCoroutine("CountDown");
    }

    private void OnDisable()
    {
        
    }

    IEnumerator CountDown()
    {
        for (int i = 0; i < numCountDown; i++)
        {
            countdown.text = (numCountDown - i).ToString();
            yield return new WaitForSeconds(1);
        }
        OnCountDownFinished();
    }
}
